plugins {
    id("pl.allegro.tech.build.axion-release") version "1.18.3"
    application
}

repositories {
    mavenCentral()
}

application {
    mainClass.set("axion.release.example.App")
}

tasks.test {
    useJUnitPlatform()
}
